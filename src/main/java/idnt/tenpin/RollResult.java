package idnt.tenpin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class RollResult {

  private boolean success;
  private int frame;



}
