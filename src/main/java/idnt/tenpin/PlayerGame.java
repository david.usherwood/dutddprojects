package idnt.tenpin;

public class PlayerGame {

  private static final int MIN_PINS = 0 ;
  private static final int MAX_PIN = 10;
  public RollResult roll(int noOfPins) {

    boolean success = MIN_PINS <= noOfPins && noOfPins <= MAX_PIN;

    return new RollResult(success, 1);
  }
}
