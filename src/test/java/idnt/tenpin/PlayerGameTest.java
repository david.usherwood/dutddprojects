package idnt.tenpin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PlayerGameTest {

  private static PlayerGame playerGame;
  @BeforeEach
  public void setup() {
   playerGame = new PlayerGame();
  }

  @ParameterizedTest
  @DisplayName("Confirm result is true where pins dropped >= 0 and <= 10")
  @MethodSource("confirmResultTrueArgs")
  public void confirmResultTrue(int pins) {
    RollResult result = playerGame.roll(pins);
    assertTrue(result.isSuccess());
  }

  @ParameterizedTest
  @DisplayName("Confirm result is false where pins dropped < 0 and > 10")
  @MethodSource("confirmResultFalseArgs")
  public void confirmResultFalse(int pins) {
    RollResult result = playerGame.roll(pins);
    assertFalse(result.isSuccess());
  }

  private static Stream<Arguments> confirmResultFalseArgs() {
    return Stream.of(
        Arguments.of(-5),
        Arguments.of( 21),
        Arguments.of( 11));
  }
  private static Stream<Arguments> confirmResultTrueArgs() {
    return Stream.of(
        Arguments.of(1),
        Arguments.of( 10),
        Arguments.of( 0));
  }
}
